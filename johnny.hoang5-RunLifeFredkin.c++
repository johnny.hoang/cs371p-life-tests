#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

#include <sstream>
using namespace std;

#include "Life.h"
#include "FredkinCell.h"


int main() {
    string s;
    getline(cin, s);
    int test_cases = stoi(s, nullptr, 10);
    getline(cin, s);
    int rows;
    int cols;
    int numCells;
    int simulations;
    int frequency;
    //For each test case to simulate
    for(int i = 0; i < test_cases; i++) {
        //Initialize the life world
        getline(cin, s);
        istringstream sin(s);
        sin >> rows >> cols;
        Life<FredkinCell> world(rows, cols);

        getline(cin, s);
        numCells = stoi(s, nullptr, 10);
        //For each cell to be added
        for(int j = 0; j < numCells; j++) {
            getline(cin, s);
            istringstream sin3(s);
            int x;
            int y;
            sin3 >> x >> y;            
            world.initCell(x,y);
        }
        getline(cin, s);
        istringstream sin2(s);
        sin2 >> simulations >> frequency;
        cout << "*** Life<FredkinCell> " << rows << "x" << cols << " ***" << endl << endl;
        cout << world << endl;
        for(int i = 1; i <= simulations; i++) {
            world.simulate();
            if(i % frequency == 0) {
                cout << world;
                cout << endl;
            }
        }
        //Empty line between test cases
        getline(cin, s);
    }
    return 0;
}
